﻿using System;

namespace DebugFor
{
    class DebugFor
    {
        static void Main()
        {
            var numero1 = 548745184;
            var numero2 = 25145;
            Int64 result = 0;
            for (int i = 0; i < numero2; i++)
            {
                result += numero1;
            }s

            Console.WriteLine("la multiplicació de {0} i {1} es {2}", numero1, numero2, result);
        }

    }
}
