﻿using System;

namespace MathDepu
{
    class MathDepu
    {
        static void Main()
        {
            var result = 0.0;
            for (int i = 0; i < 10000; i++)
            {
                if (result > 100)
                {
                    result = Math.Sqrt(result);
                }

                if (result < 0)
                {
                    result += result * result;
                }

                result += 20.2;
            }

            Console.WriteLine($"el resultat és {result}");
        }

    }
}
